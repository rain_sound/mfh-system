const Sequelize = require('sequelize')
const UserModel             = require('./models/user.model')
const PersonModel           = require('./models/person.model')
const ProductModel          = require('./models/product.model')
const CategoryModel         = require('./models/category.model')
const MaintenanceModel      = require('./models/maintenance.model')
const CommentModel          = require('./models/comment.model')
const AreaModel             = require('./models/area.model')
const ProductMainModel      = require('./models/prod_mainte.model')

const cf = require('../config')

//Configuracion inicial
const conn = new Sequelize(cf.db_name, cf.user_db, cf.password_db, {
    host: cf.host,
    dialect: cf.dialect,
    pool: {
        max: cf.max_conn,
        min: cf.min_conn
    }
})

//Iniciar modelos | ORM 
const User              = UserModel(conn, Sequelize)
const Person            = PersonModel(conn, Sequelize)
const Category          = CategoryModel(conn, Sequelize)
const Maintenance       = MaintenanceModel(conn, Sequelize)
const Comment           = CommentModel(conn, Sequelize)
const Product           = ProductModel(conn, Sequelize)
const Area              = AreaModel(conn, Sequelize)
const ProductMain       = ProductMainModel(conn, Sequelize)

Person.belongsTo(Area)
Area.hasMany(Person)

Person.hasMany(Product)
Product.belongsTo(Person)

Category.hasMany(Product)
Product.belongsTo(Category)



Product.belongsToMany(Maintenance, {
    through: ProductMain,
    foreignKey: 'product_id'
})

Maintenance.belongsToMany(Product, {
    through: ProductMain,
    foreignKey: 'maintenance_id'
})

Comment.belongsToMany(Maintenance, {
    through: ProductMain,
    foreignKey: 'comment_id'
})

//Force === true Para crear todas las tablas cuidado dropea toda la data
//Force === false Para crear todas las tablas que no existe

conn.sync({ force: false })
    .then(() => {
        console.log('Tablas y database creados')
    })
    .catch(err => {
        console.log(err);
    })

module.exports = {
    User, Person, Category, Maintenance, Comment, Product, Area, ProductMain
}