module.exports = (sequelize, type) => {
    return sequelize.define('people', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: type.STRING(80)
        },
        surname: {
            type: type.STRING(80)
        },
        code: {
            type: type.STRING(8),
            unique: {
                args: true
            }
        },
        cellphone: {
            type: type.STRING(9)
        }
    })
}