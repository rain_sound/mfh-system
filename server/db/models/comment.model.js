module.exports = (sequelize, type) => {
    return sequelize.define('comments', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: type.STRING(80),
        }
       
    })
}