module.exports = (sequelize, type) => {
    return sequelize.define('maintenance', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: type.STRING(50),
        },
        date: {
            type: type.DATEONLY                    
        }
    })
}