module.exports = (sequelize, type) => {
    return sequelize.define('products', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: type.STRING(80),
            unique: true
        },
        score: {
            type: type.INTEGER,
            validate: {
                min: 1,
                max: 5
            }
        }
    })
}