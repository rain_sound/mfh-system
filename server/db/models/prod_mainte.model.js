module.exports = (sequelize, type) => {
    return sequelize.define('prod_mainte', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        date: type.DATEONLY
    })
}