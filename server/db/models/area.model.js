module.exports = (sequelize, type) => {
    return sequelize.define('areas', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: {
            type: type.STRING(50)
        }
    })
}