const faker = require("faker");
const bcrypt = require("bcryptjs")
const { User, Person, Category, Maintenance, Comment, Product, Area, ProductMain } = require("../../db");

i = 0;
for(z = 0; z < 5; z++){
    Area.create({
        description: faker.random.word()
    })
    for(y = 0; y < 10; y++){
        Person.create({
            areaId: y,
            name: faker.name.firstName(),
            surname: faker.name.lastName(),
            code: faker.random.number(),
            cellphone: faker.random.number()
        })
    }
}
for(i = 0; i < 15; i++){
    Category.create({
        name: faker.name.firstName()
    })
    for( j = 0; j < 10; j++){
        Product.create({
            personId: faker.random.arrayElement([32,33,34,35,42,43,45,46]),
            categoryId: i,
            description: faker.random.word(),
            score: faker.random.arrayElement([1,2,3,4,5])
        })
        
    }
}

for(i = 0; i < 15; i++){
    Maintenance.create({
        description: faker.random.word(),
        date: faker.date.future()
    })
}

for(i = 0; i < 15; i++){
    Comment.create({
        description: faker.random.word(),
    })
}

for(i = 0; i < 15; i++){
    ProductMain.create({
        date: faker.date.future(),
        product_id: faker.random.arrayElement([34,42,48,50,87]),
        maintenance_id: faker.random.arrayElement([1,2,3,4,5,6,7]),
        comment_id: faker.random.arrayElement([1,2,3,4,5,6,7]),
    })
}