var router = require('express').Router()
    //Definir rutas
const users             = require('./users')
const categories        = require('./category')
const areas             = require('./area')
const employee          = require('./employee')
const products          = require('./product')


//Controladores
router.use('/users',            users)
router.use('/categories',       categories)
router.use('/areas',            areas)
router.use('/employee',         employee)
router.use('/products',         products)

module.exports = router