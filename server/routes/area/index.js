const router = require("express").Router();

const areaService = require("./area.service");

router.get("/",                  areaService.getAll);
router.get("/:id",               areaService.getById);
router.get("/:id/users",         areaService.getUsersById);

router.post("/new",              areaService.newArea);
router.put("/:id/update",        areaService.updateArea);

module.exports = router;