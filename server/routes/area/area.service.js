const { upload, sendJson } = require("../../utilities");
const { Area, Person } = require("../../db");
const config = require("../../config");



const getAll = (req, res, next) => {
    Area.findAll().then(data => sendJson(res, "", false, false, data ))
}

const getById = (req, res, next) => {
    Area.findOne({
        where: {
            id: req.params.id
        }
    }).then( data => sendJson(res, "", false, false, data ))    
}

const getUsersById = (req, res, next) => {
    Person.findAll({
        where: {
            areaId: req.query.id
        }
    }).then( data => sendJson(res, "", false, false, data ))
    .catch(err => sendJson(res, "existe un error"))    
}

const newArea = (req, res, next) => {
    if(!req.body.description) return sendJson(res, "No se enviaron datos")
    Area.create({
        description: req.body.description
    }).then(data => sendJson(res, "Se creo con éxito", false))
    .catch(err => sendJson(res, "Error al crear"))
}

const updateArea = (req, res, next) => {
    if(!req.params.id) return sendJson(res, "No se enviaron datos")
    Area.findOne({
        where: {
            id: req.params.id
        }
    }).then(Area => {
        if(Area){
            Area.update({
                description: req.body.description
            }).then(data => sendJson(res, "Se actualizó con éxito",false))
            .catch(err => sendJson(res, "Error en la actualización"))
        }
    })
    .catch(err => sendJson(res, "Error en la actualización"))
}

const deleteArea = (req, res, next) => {

}


module.exports = {
    getAll, 
    getById,
    getUsersById,
    newArea,
    updateArea,
    deleteArea
}