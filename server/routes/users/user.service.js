const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const multer = require("multer");

const { upload, sendJson } = require("../../utilities");
const { User } = require("../../db");
const config = require("../../config");

//TODO:
// 1) Verificar token
// 2) Verificar mensaje de errores upload image
// END TODO:

const getAll = (req, res, next) => {
  User.findAll().then(users => res.json(users));
};

const getById = (req, res, next) => {
  res.send(`El usuario consultado es ${req.params.id}`);
};

const signup = (req, res, next) => {
  User.create({
    name: req.body.username,
    email: req.body.email,
    imgProfile: "default.png",
    password: bcrypt.hashSync(req.body.password, 8)
  })
    .then(user => {
      return sendJson(res, "Se registro con exito", false);
    })
    .catch(err => {
      return sendJson(res,  err.original.detail);
    });
  
};

const signin = (req, res, next) => {
  User.findOne({
    where: {
      name: req.body.username
    }
  })
    .then(user => {
      //Si no existe el usuario
      if (!user) {
        return sendJson(res, "No existe usuario" );
      }
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      //Si existe error en la contrasenia
      if (!passwordIsValid) {
        return sendJson(res, "Contrasenia invalida");
      }

      //Se genera el token con fecha de expiracion
      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: "1d"
      });

      res.status(200).json({
        auth: true,
        email: user.email,
        name: user.name,
        accessToken: token,
        id: user.id,
        imgProfile: user.imgProfile
      });
    })
    .catch(err => {
      return sendJson(res, err);
    });
  
};

//verifica que el link sea validado y envia el name para la actualizacion
const resetPassword = (req, res, next) => {
  User.findOne({
    where: {
      passwordResetToken: req.query.token,
      passwordResetTokenExpire: {
        $gt: Date.now()
      }
    }
  })
    .then(user => {
      if (user) return res.status(200).json({ name: user.name, status: "ok" });
      return sendJson(res, "El link es invalido o el token expiro");
    })
    .catch(err => sendJson(res, err));
};

//envia la url con el token
const forgotPassword = (req, res, next) => {
  User.findOne({
    where: {
      email: req.body.email
    }
  })
    .then(user => {
      if (user) {
        //Por verse
        var token = bcrypt.hashSync(req.body.email, 8);

        user.update({
          passwordResetToken: token,
          passwordResetTokenExpire: Date.now() + 3600000
        });

        send_email(token, user, res);
      } else {
        return sendJson(res, "No existe cuenta asociada");
      }
    })
    .catch(err => sendJson(res, err));

};

//Tengo que enviarle el name recuperado de la anterior ruta
const updatePassword = (req, res, next) => {
  User.findOne({
    where: {
      name: req.body.username
    }
  })
    .then(user => {
      const { oldPassword } = req.body;
      // IMPORTANT:
      // Si existe el campo oldPassword se utiliza para actualizar desde profile
      if (oldPassword) {
        const matchPassword = bcrypt.compareSync(oldPassword, user.password);

        if (!matchPassword) {
          return sendJson(res, "Contraseña no coincide");
        }
      }
      // Existen dos flujos, cuando se actualiza desde recuperacion o desde profile
      if (user) {
        user
          .update({
            password: bcrypt.hashSync(req.body.password, 8),
            passwordResetToken: null,
            passwordResetTokenExpire: null
          })
          .then(() => sendJson(res, "Contraseña fue actualizada", false))
          .catch(err => sendJson(res, err));
      } else {
        return sendJson(res, "No existe usuario");
      }
    })
    .catch(err => sendJson(res, err));

 
};

function uploadImage(req, res, next) {
  upload(req, res, err => {
    if (err) {
      if (err instanceof multer.MulterError || err.code === 'LIMIT_FILE_SIZE') {
        return sendJson(res, "Archivo muy grande");
      } else {
        return sendJson(res, "Formato no permitido");
      }
    } else {
      User.findOne({
        where: {
          id: req.body.id
        }
      })
        .then(user => {
          if (user) {
            user.update({
              imgProfile: req.file.filename
            });
          } else sendJson(res, "El usuario no existe");
        })
        .catch(err => sendJson(res, err));
    }
    return res
      .status(200)
      .json({
        message: "Se actualizo la imagen con exito",
        filename: req.file.filename,
        error: false,
        alert: true
      });
  });

 
};

const send_email = (token, user, res) => {
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: config.usermail,
      pass: config.passwordmail
    }
  });

  const mailOptions = {
    from: config.usermail,
    to: user.email,
    subject: "Enlace para resetear contrasenia",
    text:
      "Ingresa al siguiente enlace para poder resetear tu contrasenia\n\n" +
      `http://localhost:3000/users/resetpassword/?token=${token}`
  };

  transporter.sendMail(mailOptions, function(err, response) {
    if (err)
      return sendJson(res, "No se pudo enviar el correo");
    return sendJson(res, "Correo de recuperacion enviado ", false);
  });
};

module.exports = {
  getAll,
  getById,
  signin,
  signup,
  resetPassword,
  forgotPassword,
  updatePassword,
  uploadImage
};
