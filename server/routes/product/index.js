const router = require("express").Router();

const productService = require("./product.service");

router.get("/",                 productService.getAll);
router.get("/:id",              productService.getById);

router.post("/new",              productService.newProduct);
router.put("/:id/update",        productService.updateProduct);

module.exports = router;