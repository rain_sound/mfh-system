const { upload, sendJson } = require("../../utilities");
const { Product } = require("../../db");
const config = require("../../config");

  
const getAll = (req, res, next) => {
    Product.findAll().then(data => sendJson(res, "", false, false, data ))
}

const getById = (req, res, next) => {
    Product.findOne({
        where: {
            id: req.params.id
        }
    }).then( data => sendJson(res, "", false, false, data ))    
}
//IMPORTANT:
//VERIFICAR EL TIPO DE DATO QUE ESTAMOS ENVIANDO
// recuerda string o int, validarlo o no se creara
//IMPORTANT:

//IMPORTANT:
// El data ponerlo al inicio para no tener que poner los valores en sendjson
//IMPORTANT:



const newProduct = (req, res, next) => {
    if(!req.body.description) return sendJson(res, "No se enviaron datos")
    Product.create({
        description:           req.body.description,
        score:                 req.body.score,
        personId:              req.body.person,
        categoryId:            req.body.category
    }).then(data => sendJson(res, "Se creo con éxito", false))
    .catch(err => sendJson(res, "Error al crear", true, true, err))
}

const updateProduct = (req, res, next) => {
    if(!req.params.id) return sendJson(res, "No se enviaron datos")
    Product.findOne({
        where: {
            id: req.params.id
        }
    }).then(Product => {
        if(Product){
            Product.update({
                description:           req.body.description,
                score:                 req.body.score,
                personId:              req.body.person,
                categoryId:            req.body.category
            }).then(data => sendJson(res, "Se actualizó con éxito",false))
            .catch(err => sendJson(res, "Error en la actualización"))
        }
    })
    .catch(err => sendJson(res, "Error en la actualización"))
}

const deleteProduct = (req, res, next) => {

}


module.exports = {
    getAll, 
    getById,
    newProduct,
    updateProduct,
    deleteProduct
}