const router = require("express").Router();

const employeeService = require("./employee.service");

router.get("/",                 employeeService.getAll);
router.get("/:id",              employeeService.getById);

router.post("/new",              employeeService.newEmployee);
router.put("/:id/update",        employeeService.updateEmployee);

module.exports = router;