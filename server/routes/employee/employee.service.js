const { upload, sendJson } = require("../../utilities");
const { Person } = require("../../db");
const config = require("../../config");

  
const getAll = (req, res, next) => {
    Person.findAll().then(data => sendJson(res, "", false, false, data ))
}

const getById = (req, res, next) => {
    Person.findOne({
        where: {
            id: req.params.id
        }
    }).then( data => sendJson(res, "", false, false, data ))    
}
//IMPORTANT:
//VERIFICAR EL TIPO DE DATO QUE ESTAMOS ENVIANDO
// recuerda string o int, validarlo o no se creara
//IMPORTANT:

//IMPORTANT:
// El data ponerlo al inicio para no tener que poner los valores en sendjson
//IMPORTANT:



const newEmployee = (req, res, next) => {
    if(!req.body.name) return sendJson(res, "No se enviaron datos")
    Person.create({
        name:           req.body.name,
        surname:        req.body.surname,
        code:           req.body.code,
        cellphone:      req.body.cellphone,
        areaId:         req.body.area
    }).then(data => sendJson(res, "Se creo con éxito", false))
    .catch(err => sendJson(res, "Error al crear", true, true, err))
}

const updateEmployee = (req, res, next) => {
    if(!req.params.id) return sendJson(res, "No se enviaron datos")
    Person.findOne({
        where: {
            id: req.params.id
        }
    }).then(Person => {
        if(Person){
            Person.update({
                name:           req.body.name,
                surname:        req.body.surname,
                code:           req.body.code,
                cellphone:      req.body.cellphone,
                areaId:         req.body.area
            }).then(data => sendJson(res, "Se actualizó con éxito",false))
            .catch(err => sendJson(res, "Error en la actualización"))
        }
    })
    .catch(err => sendJson(res, "Error en la actualización"))
}

const deletePerson = (req, res, next) => {

}


module.exports = {
    getAll, 
    getById,
    newEmployee,
    updateEmployee,
    deletePerson
}