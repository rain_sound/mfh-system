import React from 'react';
import { store } from './store'

const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard'));
const Profile = React.lazy(() => import('./components/Profile/Profile'));
const PasswordRecovery = React.lazy(() => import('./components/Profile/PasswordRecovery'));
const Employee = React.lazy(() => import('./views/Pages/Employee'))
const NewArea = React.lazy(() => import('./views/Pages/Area'))

const user = store.getState().user;

const routes = [
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/profile', exact:true, name: 'Profile', component: PasswordRecovery },
  { path: '/profile/:name/reset', name: 'Reset Password', component: PasswordRecovery },
  { path: '/profile/:name', name: 'Profile Image', component: Profile },
  { path: '/areas/:id', name: 'Areas', component: Employee },
  { path: '/new/area/', name: 'Areas', component: NewArea },
];

export default routes;
