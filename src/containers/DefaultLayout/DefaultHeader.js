import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import { URL_PUBLIC } from '../../helpers/api'
import logo from '../../assets/img/brand/logo.jpg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props){
    super(props)
  }
  render() {

    // eslint-disable-next-line
    const { children, user, ...attributes } = this.props;
    const profile = user.profile? user.profile.filename: user.profile;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 52, height: 52, alt: 'Innicia Logo' }}
          minimized={{ src: logo, width: 30, height: 30, alt: 'Innicia Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Innicia</NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <span>Bienvenido <strong>{ this.props.user.name }</strong></span>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={`${URL_PUBLIC}${profile}`} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
              <DropdownItem onClick={e => this.props.onLogout(e)}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = ({ user }) => ({ user })

export default connect(mapStateToProps, null)(DefaultHeader);
