import { combineReducers } from 'redux'
import { user, getUserId as _getUserId, getUsername as _getUsename } from './user'
import { alert } from './alert'
import { userConstants } from '../constants/user/actions'
import { reducer as reduxForm } from 'redux-form'

 const appReducer = combineReducers({
    user,
    alert,
    form: reduxForm
})

export const rootReducer = (state, action) => {
    if (action.type === userConstants.LOGOUT) {
        state = undefined
      }
      return appReducer(state, action)
  }

//Selectors
export const getUserId = state => _getUserId(state.user)
export const getUsername = state => _getUsename(state.user)