import { alertConstants } from '../constants/alerts/actions'
import { createToast } from '../helpers/alerts'

const addAlert    = ({ error, message }, { id }) => {
    const request = payload => ({ type: alertConstants.ADD_TOAST, payload: createToast(payload, id) })

    return dispatch => {
        if(error) dispatch(request({ message, type: 'error'}, id))
        else  dispatch(request({ message, type: 'success'}, id))
    }
}

const removeAlert = id => {
    const request = id      => ({ type: alertConstants.REMOVE_TOAST, payload: id })
    return dispatch => {
        dispatch(request(id))
    }
}

const clearAlert = () => {
    const request = ()     => ({ type: alertConstants.CLEAR_ALL_TOAST })
    return dispatch => {
        dispatch(request())
    }
}

export const alertActions = {
    addAlert,
    removeAlert,
    clearAlert
};