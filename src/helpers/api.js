import axios from 'axios';
import { authHeader } from './auth_header'
import { Service } from 'axios-middleware';
import { alertActions } from '../actions/alert'
import { store } from '../store/index'
import { generateId } from '../helpers/alerts'
//Se utiliza cuando se quieren hacer solicitudes con form-data
export const configFormData = { headers: { "Content-Type": "multipart/form-data" } };

export const URL = 'http://localhost:3030/'
export const URL_PUBLIC = 'http://localhost:3030/public/'


const client = axios.create({
  baseURL: URL,
  headers: authHeader(),
  responseType: 'json', 
})

const service = new Service(client);
service.register({
  onResponse(response) {
    const { data } = response; 
    const id = generateId()
    if(data.alert){
      store.dispatch(alertActions.addAlert(data, {id}))
    }
    return data;
  }
});

export default client;