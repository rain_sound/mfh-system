import { store } from '../store/index'
import { alertActions } from '../actions/alert'

export const generateId = () => {
  const today = new Date().toISOString()
  setTimeout(() => {
    store.dispatch(alertActions.removeAlert(today))
  }, 3500)
  return today
}

export const createToast = (options,id) => {
  return {
    ...options,
    id
  }
}

