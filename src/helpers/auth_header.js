
export const authHeader = () => {
    let user = localStorage.getItem('user')
    if(user && user.accessToken) {
        return {
            'access-token': user.accessToken
        }
    }
}
