import { store } from './store/index'

const user = store.getState().user;

export default {
  items: [
    {
      name: `Bienvenido ${user.name}`,
      icon: `icon-mustache`,
      attributes: { disabled: true },
      badge: {
        variant: 'success',
        text: 'Online',
      },
    },
    {
      name: 'Profile',
      url: '/profile',
      icon: 'icon-user',
      children: [
        {
          name: 'Password',
          url: `/profile/${user.name}/reset`,
          icon: 'icon-wrench',
        },
        {
          name: 'Profile Image',
          url: `/profile/${user.name}`,
          icon: 'icon-camera',
        },
      ],
    },
    {
      name: 'Areas',
      url: '/areas',
      icon: 'icon-user',
      children: [
        {
          name: 'Crear area',
          url: '/new/area/',
          icon: 'icon-wrench',
        },
        {
          name: 'Editar area',
          url: `/profile/${user.name}`,
          icon: 'icon-camera',
        },
      ],
    },
  ],
};
