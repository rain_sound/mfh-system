import { URL } from '../../helpers/api'

const GETALL            = `${URL}employee/` 
const GETBYID           = `${URL}employee/:id` 
const NEWAREA           = `${URL}employee/new` 
const UPDATEAREA        = `${URL}employee/:id/update` 

export const areaConstants = {
    GETALL,
    GETBYID,
    NEWAREA,
    UPDATEAREA
}