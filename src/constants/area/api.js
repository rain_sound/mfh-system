import { URL } from '../../helpers/api'

const GETALL                = `${URL}areas/` 
const GETBYID               = `${URL}areas/:id` 
const GETUSERSBYID          = `${URL}areas/:id/users` 
const NEWAREA               = `${URL}areas/new` 
const UPDATEAREA            = `${URL}areas/:id/update` 

export const areaConstants = {
    GETALL,
    GETBYID,
    NEWAREA,
    UPDATEAREA,
    GETUSERSBYID
}