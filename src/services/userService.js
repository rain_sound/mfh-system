import api from "../helpers/api";
import { configFormData } from "../helpers/api";
import { userConstants } from "../constants/user/api";

const login = (username, password) => {
  return api
    .post(userConstants.SIGNIN, {
      username,
      password
    })
    .then(user => {
      console.log('respuesta==', user);
      localStorage.setItem("user", JSON.stringify(user.data));
      return user;
    })
};

const register = (username, password, email) => {
  return api
    .post(userConstants.SIGNUP, {
      username,
      email,
      password
    })
    .then(user => {
      return user;
    })
};

const resetPassword = email => {
  return api
    .post(userConstants.FORGOTPASSWORD, {
      email
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason, message } = res.data;
      return {
        error,
        reason,
        message
      };
    });
};

const verifyToken = token => {
  return api
    .get(userConstants.RESETPASSWORD, {
      params: {
        token
      }
    })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason } = res.data;
      return {
        error,
        reason
      };
    });
};

const updatePassword = (username, password, oldPassword) => {
  return api
    .put(userConstants.UPDATEPASSWORD, {
      username,
      oldPassword,
      password
    })
    .then(res => {
      return res;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason } = res.data;
      return {
        error,
        reason
      };
    });
};

const updateImage = (image, id) => {
  const formData = new FormData();
  formData.append("image", image);
  formData.append("id", id);
  return api
    .post(userConstants.UPDATEIMAGE, formData, configFormData)
    .then(res => {
      return res;
    })
    .catch(err => {
      const res = err.response;
      const { error, reason } = res.data;
      return {
        error,
        reason
      };
    });
};

const getAll = () => {
  return api
    .get(userConstants.GET_ALL)
    .then(res => console.log(res))
    .catch(err => console.log(err));
};

export const userService = {
  getAll,
  login,
  register,
  resetPassword,
  verifyToken,
  updatePassword,
  updateImage
};
