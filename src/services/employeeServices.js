import api from "../helpers/api";
import { areaConstants } from "../constants/area/api";

const getAll = () => {
  return api
    .get(areaConstants.GETALL)
    .then(res => res.data);
};

export const areaServices = {
  getAll
//   getById,
//   createArea,
//   updateArea
};
