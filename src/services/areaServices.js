import api from "../helpers/api";
import { areaConstants } from "../constants/area/api";

const getAll = () => {
  return api.get(areaConstants.GETALL).then(res => {
    return res;
  });
};

const getUsersById = id => {
  return api
    .get(areaConstants.GETUSERSBYID, {
      params: {
        id
      }
    })
    .then(res => res);
};

const createArea = description => {
  return api
    .post(areaConstants.NEWAREA, {
      description
    })
    .then(res => {
      return res;
    });
};

export const areaServices = {
  getAll,
  getUsersById,
  createArea
  //   getById,
  //   updateArea
};
