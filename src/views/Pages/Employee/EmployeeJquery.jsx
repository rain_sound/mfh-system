import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row
} from "reactstrap";

import update from "immutability-helper";

import { areaServices } from "../../../services/areaServices";

import $ from "jquery";
import DataTable from "datatables.net";

$.DataTable = DataTable;

const columns = [
  {
    data: "code",
    title: "Codigo"
  },
  {
    data: "name",
    title: "Nombre"
  },
  {
    data: "surname",
    title: "Apellido"
  },
  {
    data: "cellphone",
    title: "Celular"
  }
];

class Employee extends Component {
  constructor() {
    super();

    this.state = {
      employees: null,
      modalUpdate: false,
      modalDelete: false,
      code: null,
      name: null,
      surname: null,
      cellphone: null
    };
  }

  componentDidMount = () => {
    const { id } = this.props.match.params;
    areaServices.getUsersById(id).then(employees => {
      this.setState({ employees: employees.data });
      $(this.refs.main).DataTable({
        dom: '<"data-table-wrapper"t>',
        data: employees.data,
        columns,
        retrieve: true,
        ordering: true,
        dom: "Bfrtip",
        bPaginate: true,
        buttons: [
          'copyHtml5', 'excel', 'pdfHtml5', 'csvHtml5'
        ],
        columnDefs: [
          {
            searchable: false,
            orderable: false,
            targets: 0
          }
        ],
        order: [[1, "asc"]]
      });
    });
  };

  componentWillUnmount() {
    $(".data-table-wrapper")
      .find("table")
      .DataTable()
      .destroy(true);
  }
  shouldComponentUpdate() {
    return false;
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //IMPORTANT:
  // Modal Methods
  toggleUpdate = ({ name = "", surname = "", cellphone = "", code = "" }) => {
    this.setState({
      modalUpdate: !this.state.modalUpdate,
      name,
      surname,
      cellphone,
      code
    });
  };

  toggleDelete = ({ code }) => {
    this.setState({ modalDelete: !this.state.modalDelete, code });
  };
  //Modal Methods
  //IMPORTANT:

  //IMPORTANT:
  // Update row after update click button
  updateDataTable = e => {
    e.preventDefault();
    const { employees, code, name, surname, cellphone } = this.state;
    employees.map((item, index) => {
      if (item.code === code) {
        this.setState({
          employees: update(employees, {
            [index]: {
              name: { $set: name },
              surname: { $set: surname },
              cellphone: { $set: cellphone }
            }
          })
        });
      }
    });
  };

  render() {
    const { employees, code, name, surname, cellphone } = this.state;
    return (
      <div className="row">
        <table ref="main" />
        {/* Modal Delete */}
        <Modal isOpen={this.state.modalDelete} toggle={this.toggleDelete}>
          <ModalHeader toggle={this.toggle}>Modal Delete</ModalHeader>
          <ModalBody>Este es un modal delete</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleDelete}>
              Do Something
            </Button>
            <Button color="secondary" onClick={this.toggleDelete}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>

        {/* Modal Update */}
        <Modal isOpen={this.state.modalUpdate} toggle={this.modalUpdate}>
          <ModalHeader className="bg-info color-white" toggle={this.toggle}>
            Actualizar Informacion
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12">
                <Card>
                  <CardBody>
                    <Form action="" method="post">
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Codigo
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="code"
                            id="code"
                            disabled
                            value={code}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Nombre
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="name"
                            id="name"
                            value={name}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Apellido
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="surname"
                            id="surname"
                            value={surname}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-asterisk" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Celular
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="cellphone"
                            id="cellphone"
                            value={cellphone}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-asterisk" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup className="form-actions text-center">
                        <Button
                          type="submit"
                          color="primary"
                          onClick={this.updateDataTable}
                        >
                          <i className="fa fa-dot-circle-o" /> Actualizar
                        </Button>
                        <Button
                          type="reset"
                          color="danger"
                          onClick={this.toggleUpdate}
                        >
                          <i className="fa fa-ban" /> Limpiar
                        </Button>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default Employee;
