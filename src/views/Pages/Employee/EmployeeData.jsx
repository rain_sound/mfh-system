import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row
} from "reactstrap";

import update from "immutability-helper";

import { areaServices } from "../../../services/areaServices";

import "react-tabulator/lib/styles.css"; // required styles
import "react-tabulator/lib/css/tabulator.min.css"; // theme
import "react-tabulator/css/bootstrap/tabulator_bootstrap.min.css"; // use Theme(s)
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import ToolkitProvider, {  CSVExport,  Search } from "react-bootstrap-table2-toolkit";

// for React 16.4.x use: import { ReactTabulator }

import BootstrapTable from "react-bootstrap-table-next";
import ButtonRow from "../../../components/Other/Table/ButtonRow";

class Employee extends Component {
  constructor() {
    super();
    this.UpdateButton = this.UpdateButton.bind(this);
    this.DeleteButton = this.DeleteButton.bind(this);

    this.state = {
      employees: null,
      modalUpdate: false,
      modalDelete: false,
      code: null,
      name: null,
      surname: null,
      cellphone: null,
      columns: [
        {
          dataField: "code",
          text: "Codigo",
          sort: true,
          onSort: (field, order) => {
            console.log("field", field, "order", order);
          }
        },
        {
          dataField: "name",
          text: "Nombre",
          sort: true
        },
        {
          dataField: "surname",
          text: "Apellido",
          sort: true
        },
        {
          dataField: "cellphone",
          text: "Celular",
          sort: true
        },
        {
          dataField: "actionsUpdate",
          text: "Actions",
          csvExport: false,
          isDummyField: false,
          formatter: this.UpdateButton,
          style: { textAlign: "center" }
        },
        {
          dataField: "actionsDelete",
          text: "Actions",
          isDummyField: false,
          csvExport: false,
          formatter: this.DeleteButton,
          style: { textAlign: "center" }
        }
      ]
    };
  }

  componentDidMount = () => {
    const { id } = this.props.match.params;
    areaServices
      .getUsersById(id)
      .then(employees => this.setState({ employees: employees.data }));
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //IMPORTANT:
  // Modal Methods
  toggleUpdate = ({ name = "", surname = "", cellphone = "", code = "" }) => {
    this.setState({
      modalUpdate: !this.state.modalUpdate,
      name,
      surname,
      cellphone,
      code
    });
  };

  toggleDelete = ({ code }) => {
    this.setState({ modalDelete: !this.state.modalDelete, code });
  };
  //Modal Methods
  //IMPORTANT:

  //IMPORTANT:
  // Update row after update click button
  updateDataTable = e => {
    e.preventDefault();
    const { employees, code, name, surname, cellphone } = this.state;
    employees.map((item, index) => {
      if (item.code === code) {
        this.setState({
          employees: update(employees, {
            [index]: {
              name: { $set: name },
              surname: { $set: surname },
              cellphone: { $set: cellphone }
            }
          })
        });
      }
    });
  };
  //IMPORTANT:
  //BUTTON COLUMN TABLE
  UpdateButton(cell, row) {
    return (
      <ButtonRow
        title="Actualizar"
        toggle={this.toggleUpdate}
        row={row}
        width={"w-75"}
        type="success"
      />
    );
  }

  //BUTTON COLUMN TABLE
  DeleteButton(cell, row) {
    return (
      <ButtonRow
        title="Borrar"
        toggle={this.toggleDelete}
        row={row}
        width={"w-75"}
        type="danger"
      />
    );
  }

  render() {
    const { employees, code, name, surname, cellphone } = this.state;
    const { ExportCSVButton } = CSVExport;
    const { SearchBar, ClearSearchButton } = Search;
    return (
      <div className="row">
        <div className="col-auto">
          {employees && (
            <ToolkitProvider
              keyField={"id"}
              classes="card table-responsive"
              data={employees}
              columns={this.state.columns}
              search
              exportCSV={{
                onlyExportFiltered: true,
                ignoreHeader: true,
                exportAll: false
              }}
            >
              {props => (
                <div className="card">
                  <div className="card-header">
                    <div className="row">
                      <div className="col-3">
                        <SearchBar tableId="id" {...props.searchProps} />
                      </div>
                      <div className="col-auto">
                        <ClearSearchButton
                          className="btn btn-secondary"
                          {...props.searchProps}
                        />
                      </div>
                      <div className="col-auto">
                        <ExportCSVButton
                          className="btn btn-success"
                          {...props.csvProps}
                        >
                          Export CSV
                        </ExportCSVButton>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-12">
                      <BootstrapTable
                        hover
                        striped
                        {...props.baseProps}
                        rowEvents={this.rowEvents}
                      />
                    </div>
                  </div>
                </div>
              )}
            </ToolkitProvider>
          )}
        </div>

        {/* Modal Delete */}
        <Modal isOpen={this.state.modalDelete} toggle={this.toggleDelete}>
          <ModalHeader toggle={this.toggle}>Modal Delete</ModalHeader>
          <ModalBody>Este es un modal delete</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleDelete}>
              Do Something
            </Button>
            <Button color="secondary" onClick={this.toggleDelete}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>

        {/* Modal Update */}
        <Modal isOpen={this.state.modalUpdate} toggle={this.modalUpdate}>
          <ModalHeader className="bg-info color-white" toggle={this.toggle}>
            Actualizar Informacion
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12">
                <Card>
                  <CardBody>
                    <Form action="" method="post">
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Codigo
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="code"
                            id="code"
                            disabled
                            value={code}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Nombre
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="name"
                            id="name"
                            value={name}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Apellido
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="surname"
                            id="surname"
                            value={surname}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-asterisk" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Celular
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="cellphone"
                            id="cellphone"
                            value={cellphone}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-asterisk" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup className="form-actions text-center">
                        <Button
                          type="submit"
                          color="primary"
                          onClick={this.updateDataTable}
                        >
                          <i className="fa fa-dot-circle-o" /> Actualizar
                        </Button>
                        <Button
                          type="reset"
                          color="danger"
                          onClick={this.toggleUpdate}
                        >
                          <i className="fa fa-ban" /> Limpiar
                        </Button>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default Employee;
