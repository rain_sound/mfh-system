import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Row
} from "reactstrap";

import update from "immutability-helper";
import ButtonRow from "../../../components/Other/Table/ButtonRow";

import { areaServices } from "../../../services/areaServices";

import MUIDataTable from "mui-datatables";



class Employee extends Component {
  constructor() {
    super();

    this.state = {
      employees: null,
      modalUpdate: false,
      modalDelete: false,
      code: null,
      name: null,
      surname: null,
      cellphone: null,
      options: {
        filterType: "checkbox",
        onRowsSelect: (rowsSelected, allRows) => {
          allRows.forEach(row => {
            const dataRow = this.state.employees[row.dataIndex]; // this is the row in your data source
            console.log(dataRow);
          });        
       },
      },
      columns: [
        {
          name: "name",
          label: "Nombre",
          options: {
            filter: true,
            sort: true
          }
        },
        {
          name: "surname",
          label: "Apellido",
          options: {
            filter: true,
            sort: true
          }
        },
        {
          name: "code",
          label: "Codigo",
          options: {
            filter: true,
            sort: true
          }
        },
        {
          name: "cellphone",
          label: "Cellphone",
          options: {
            filter: true,
            sort: true
          }
        }
      ]
    };
  }

  componentDidMount = () => {
    const { id } = this.props.match.params;
    areaServices.getUsersById(id).then(employees => {
      this.setState({ employees: employees.data });
    });
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //IMPORTANT:
  // Modal Methods
  toggleUpdate = ({ name = "", surname = "", cellphone = "", code = "" }) => {
    this.setState({
      modalUpdate: !this.state.modalUpdate,
      name,
      surname,
      cellphone,
      code
    });
  };

  toggleDelete = ({ code }) => {
    this.setState({ modalDelete: !this.state.modalDelete, code });
  };

  // Update row after update click button
  updateDataTable = e => {
    e.preventDefault();
    const { employees, code, name, surname, cellphone } = this.state;
    employees.map((item, index) => {
      if (item.code === code) {
        this.setState({
          employees: update(employees, {
            [index]: {
              name: { $set: name },
              surname: { $set: surname },
              cellphone: { $set: cellphone }
            }
          })
        });
      }
    });
  };

  render() {
    const { employees, code, name, surname, cellphone, columns, options } = this.state;
    return (
      <div className="row">
        <div className="col-12">
          {employees && (
            <MUIDataTable
              title={"Employee List"}
              data={employees}
              columns={columns}
              options={options}
            />
          )}
        </div>

        {/* Modal Delete */}
        <Modal isOpen={this.state.modalDelete} toggle={this.toggleDelete}>
          <ModalHeader toggle={this.toggle}>Modal Delete</ModalHeader>
          <ModalBody>Este es un modal delete</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleDelete}>
              Do Something
            </Button>
            <Button color="secondary" onClick={this.toggleDelete}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>

        {/* Modal Update */}
        <Modal isOpen={this.state.modalUpdate} toggle={this.modalUpdate}>
          <ModalHeader className="bg-info color-white" toggle={this.toggle}>
            Actualizar Informacion
          </ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12">
                <Card>
                  <CardBody>
                    <Form action="" method="post">
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Codigo
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="code"
                            id="code"
                            disabled
                            value={code}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Nombre
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="name"
                            id="name"
                            value={name}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-user" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Apellido
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="surname"
                            id="surname"
                            value={surname}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-asterisk" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend" className="w-25">
                            <InputGroupText className="w-100">
                              Celular
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            onChange={this.handleChange}
                            name="cellphone"
                            id="cellphone"
                            value={cellphone}
                          />
                          <InputGroupAddon addonType="append">
                            <InputGroupText>
                              <i className="fa fa-asterisk" />
                            </InputGroupText>
                          </InputGroupAddon>
                        </InputGroup>
                      </FormGroup>
                      <FormGroup className="form-actions text-center">
                        <Button
                          type="submit"
                          color="primary"
                          onClick={this.updateDataTable}
                        >
                          <i className="fa fa-dot-circle-o" /> Actualizar
                        </Button>
                        <Button
                          type="reset"
                          color="danger"
                          onClick={this.toggleUpdate}
                        >
                          <i className="fa fa-ban" /> Limpiar
                        </Button>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default Employee;
