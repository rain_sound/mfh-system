import React, { Component } from "react";
import CardOptionList from '../../components/Other/CardOptionList'

class Dashboard extends Component {
  constructor(props) {
    super(props);
  }
  loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  render() {
    return (
      <div className="animated fadeIn row">
        <CardOptionList />
      </div>
    );
  }
}

export default Dashboard;
