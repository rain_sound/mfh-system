import React from "react";
import { Col, Card, CardBody } from "reactstrap";

const CardOption = ({ subtitle, color, id }) => {
  return (
    <React.Fragment>
      <Col xs="6" sm="6" lg="3">
        <a href={`/areas/${id}/`} className="link--hover">
          <Card className={`text-white ${color}`}>
            <CardBody className="pb-0">
              <div className="text-value">Area</div>
              <div>{subtitle}</div>
            </CardBody>
            <div className="chart-wrapper mx-3" style={{ height: "70px" }} />
          </Card>
        </a>
      </Col>
    </React.Fragment>
  );
};

export default CardOption;
