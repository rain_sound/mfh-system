import React, { Component } from "react";
import CardOption from "./CardOption";
import { areaServices } from "../../services/areaServices";

const color = ["success", "danger", "info", "warning"];

export class CardOptionList extends Component {
  constructor() {
    super();
    this.state = {
      areas: []
    };
  }

  componentDidMount = () => {
    areaServices.getAll().then(areas => {
      this.setState({ areas });
    });
  };

  renderCardOption = () => {
    const { data } = this.state.areas;
    if (data) {
      return data.map(area => {
        let random = Math.floor(Math.random() * 4);
        return (
          <CardOption
            subtitle={area.description}
            key={area.id}
            id={area.id}
            color={`bg-${color[random]}`}
          />
        );
      });
    }
  };

  render() {
    return <React.Fragment>{this.renderCardOption()}</React.Fragment>;
  }
}

export default CardOptionList;
