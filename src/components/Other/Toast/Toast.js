import React, { Component } from "react";

let percentaje = 0

class Toast extends Component {
  render() {
    return (
      <React.Fragment>
        <div
          onClick={this.props.onDismissClick}
          className={`toast ${
            this.props.type === "error" ? "toast--error" : "toast--success"
          }`}
        >
          <p className="toast__content">{this.props.message}</p>
          <button className="toast__dismiss">x</button>
        </div>
        {/* <div className="progress" style={{ width: "100%" }}>
          <div
            className="progress-bar bg-success"
            role="progressbar"
            style={{ width: `${percentaje*10}%` }}
            aria-valuenow="25"
            aria-valuemin="0"
            aria-valuemax="100"
          />
        </div> */}
      </React.Fragment>
    );
  }

  shouldComponentUpdate() {
    return false;
  }
}

export default Toast;
