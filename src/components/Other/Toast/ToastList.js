import React from "react";
import { connect } from "react-redux";
import Toast from "./Toast";
import { alertActions } from '../../../actions/alert'

const Toasts = ({ removeAction, toasts }) => {
  return (
    <div className="toasts">
      {toasts.map(toast => {
        const { id } = toast;
        return (
          <Toast {...toast} key={id} onDismissClick={() => removeAction(id)} />
        );
      })}
    </div>
  );
};


const mapDispatchToProps = dispatch => ({
  removeAction: id => dispatch(alertActions.removeAlert(id)) 
});

const mapStateToProps = state => ({
  toasts: state.alert.toast
});

export default connect(mapStateToProps, mapDispatchToProps)(Toasts)
